Notes:

1. Player avatar is constant, means no metter what url you choose, it will show the same avatar.
2. I used Picasso to show avatar from image link.
3. I used GSON to serialize and deserialize Objects to or from json objects.
4. The main screen is showing the players list with name and avatar
5. You can add new players in the main screen.
6. To remove player you need to enter player's details screen and click on remove.
7. To rank player you need to enter player's details screen and set rank end email.
8. Valid rank is rank in range of 1-5.
9. There is no email validation.
10. Each entrance to main screen load's the players list from server.
11. Server's mock class has less nicer code.
12. Determine if user has already ranked player by email. <Email, playerId>
13. Mock server sums total rank of player and count of ranks.