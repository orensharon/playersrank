package com.orensharon.playersranking;

/**
 * Created by orensharon on 7/6/17.
 */

public class Constants {
    public final static String PLAYER_ID_EXTRA = "PLAYER_ID_EXTRA";

    public final static String GET_PLAYERS_LIST_URL = "/player";
    public final static String RANK_PLAYER_URL = "/rank";
    public final static String ADD_PLAYER_URL = "/player";
    public final static String REMOVE_PLAYER_URL = "/player";
    public final static String GET_PLAYER_URL = "/player";

    public final static String AVATAR_URL = "https://www.timeshighereducation.com/sites/default/files/byline_photos/default-avatar.png";
}
