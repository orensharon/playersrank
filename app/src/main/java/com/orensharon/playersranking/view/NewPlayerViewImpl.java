package com.orensharon.playersranking.view;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.EditText;

import com.orensharon.playersranking.R;
import com.orensharon.playersranking.model.Player;

/**
 * Created by orensharon on 7/5/17.
 */

public class NewPlayerViewImpl implements NewPlayerView {

    private View mRootView;
    private AddNewPlayerClickListener mListener;

    // Views
    private EditText mPlayerNameEditText, mPlayerPictureUrlEditText;
    private Button mSubmitButton;

    public NewPlayerViewImpl(LayoutInflater inflater, ViewGroup container) {
        mRootView = inflater.inflate(R.layout.view_add_new_player, null, false);
        container.addView(mRootView);

        // Initialize view components
        initialize();

        // Add new players
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    String playerName, playerPictureUrl;
                    playerName = mPlayerNameEditText.getText().toString();
                    playerPictureUrl = mPlayerPictureUrlEditText.getText().toString();
                    if (!playerName.isEmpty() && !playerPictureUrl.isEmpty()) {
                        mListener.onAddNewPlayerClicked(playerName, playerPictureUrl);
                    }
                }
            }
        });
    }

    private void initialize() {
        mPlayerNameEditText = (EditText) mRootView.findViewById(R.id.player_name);
        mPlayerPictureUrlEditText = (EditText) mRootView.findViewById(R.id.player_picture);
        mSubmitButton = (Button) mRootView.findViewById(R.id.submit);
    }

    @Override
    public void bind(Player player) {

    }

    @Override
    public void setListener(AddNewPlayerClickListener listener) {
        mListener = listener;
    }

    @Override
    public View getRootView() {
        return mRootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }
}
