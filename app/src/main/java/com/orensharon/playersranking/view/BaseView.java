package com.orensharon.playersranking.view;

import android.os.Bundle;
import android.view.View;

/**
 * Created by orensharon on 7/5/17.
 */

public interface BaseView {
    View getRootView();
    Bundle getViewState();
}
