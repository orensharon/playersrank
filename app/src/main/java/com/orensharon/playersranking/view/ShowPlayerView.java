package com.orensharon.playersranking.view;

import com.orensharon.playersranking.model.Player;

/**
 * Created by orensharon on 7/5/17.
 */

public interface ShowPlayerView extends BaseView {

    interface ShowPlayerClickEventsListener {
        void onRankPlayerClicked(Player player, String email);
        void onPlayerRemovedClicked(String playerId);
    }

    void bindPlayer(Player player);
    void setListener(ShowPlayerClickEventsListener listener);
}
