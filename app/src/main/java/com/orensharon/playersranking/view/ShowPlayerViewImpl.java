package com.orensharon.playersranking.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.orensharon.playersranking.R;
import com.orensharon.playersranking.model.Player;
import com.squareup.picasso.Picasso;

import static com.orensharon.playersranking.Constants.AVATAR_URL;

/**
 * Created by orensharon on 7/5/17.
 */

public class ShowPlayerViewImpl implements ShowPlayerView {

    private View mRootView;

    // Views
    private Button mSubmitRankButton, mRemovePlayerButton;
    private EditText mYourRankEditText, mEmailEditText;
    private TextView mPlayerNameTextView, mPlayerRankTextView;
    private ImageView mPlayerAvatar;

    private ShowPlayerClickEventsListener mShowPlayerClickEventsListener;

    private Player mPlayer;

    public ShowPlayerViewImpl(LayoutInflater inflater, ViewGroup container) {
        mRootView = inflater.inflate(R.layout.view_show_player, null, false);
        container.addView(mRootView);

        // Initialize view components
        initialize();
    }

    private void initialize() {
        mPlayerNameTextView = (TextView) mRootView.findViewById(R.id.player_name);
        mPlayerRankTextView = (TextView) mRootView.findViewById(R.id.rank);
        mPlayerAvatar = (ImageView) mRootView.findViewById(R.id.avatar);

        mYourRankEditText = (EditText) mRootView.findViewById(R.id.your_rank);
        mEmailEditText = (EditText) mRootView.findViewById(R.id.email);
        mSubmitRankButton = (Button) mRootView.findViewById(R.id.submit_rank);
        mRemovePlayerButton = (Button) mRootView.findViewById(R.id.remove);

        // Player rank click listener
        mSubmitRankButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mShowPlayerClickEventsListener != null) {
                    int userRank = 0;
                    String email = mEmailEditText.getText().toString();
                    try {
                        userRank = Integer.parseInt(mYourRankEditText.getText().toString());
                        mPlayer.setPlayerRank(userRank);
                    } catch (NumberFormatException ex) {
                    } finally {
                        mShowPlayerClickEventsListener.onRankPlayerClicked(mPlayer, email);
                    }
                }
            }
        });

        // Remove player click listener
        mRemovePlayerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mShowPlayerClickEventsListener  != null) {
                    mShowPlayerClickEventsListener .onPlayerRemovedClicked(mPlayer.getPlayerId());
                }
            }
        });

    }

    @Override
    public void bindPlayer(Player player) {
        mPlayer = player;
        mPlayerNameTextView.setText(player.getPlayerName());
        mPlayerRankTextView.setText(String.valueOf(player.getPlayerRank()));

        Picasso.with(getRootView().getContext())
                .load(AVATAR_URL)
                .into(mPlayerAvatar);
    }

    @Override
    public void setListener(ShowPlayerClickEventsListener listener) {
        mShowPlayerClickEventsListener = listener;
    }


    @Override
    public View getRootView() {
        return mRootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }
}
