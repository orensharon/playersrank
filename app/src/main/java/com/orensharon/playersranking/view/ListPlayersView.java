package com.orensharon.playersranking.view;

import com.orensharon.playersranking.model.Player;

import java.util.List;

/**
 * Created by orensharon on 7/5/17.
 */

public interface ListPlayersView extends BaseView {

    interface PlayerListItemListener {
        void onPlayerClicked(Player player);
    }

    interface PlayersControlListener {
        void addNewPlayerClickListener();
    }

    void bindPlayers(List<Player> player);
    void setListItemListener(PlayerListItemListener listener);
    void setPlayersControlListener(PlayersControlListener listener);
}
