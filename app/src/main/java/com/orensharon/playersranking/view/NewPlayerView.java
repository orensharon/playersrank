package com.orensharon.playersranking.view;

import com.orensharon.playersranking.model.Player;

/**
 * Created by orensharon on 7/5/17.
 */

public interface NewPlayerView extends BaseView {

    interface AddNewPlayerClickListener {
        void onAddNewPlayerClicked(String name, String picture);
    }

    void bind(Player player);
    void setListener(AddNewPlayerClickListener listener);
}
