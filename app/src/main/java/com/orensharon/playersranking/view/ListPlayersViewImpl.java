package com.orensharon.playersranking.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.orensharon.playersranking.R;
import com.orensharon.playersranking.adapter.ListPlayersAdapter;
import com.orensharon.playersranking.model.Player;

import java.util.List;

/**
 * Created by orensharon on 7/5/17.
 */

public class ListPlayersViewImpl implements ListPlayersView {


    private PlayerListItemListener mPlayerListItemListener;
    private PlayersControlListener mPlayersControlListener;

    private View mRootView;
    private RecyclerView mPlayersList;
    private ListPlayersAdapter mListPlayersAdapter;
    private Button mNewPlayerButton;

    public ListPlayersViewImpl(LayoutInflater inflater, ViewGroup container) {
        mRootView = inflater.inflate(R.layout.view_list_players, null, false);
        container.addView(mRootView);

        // Initialize view components
        initialize();

        mNewPlayerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayersControlListener != null) {
                    mPlayersControlListener.addNewPlayerClickListener();
                }
            }
        });

        // TODO: On recycle view item click - show player activity
    }

    private void initialize() {
        mNewPlayerButton = (Button) mRootView.findViewById(R.id.add_new_player);
        mPlayersList = (RecyclerView) mRootView.findViewById(R.id.players_list);
        mPlayersList.setLayoutManager(new LinearLayoutManager(getRootView().getContext()));
    }

    @Override
    public void bindPlayers(List<Player> players) {
        mListPlayersAdapter = new ListPlayersAdapter(getRootView().getContext(), players);
        mPlayersList.setAdapter(mListPlayersAdapter);
        mListPlayersAdapter.setOnItemClickListener(new ListPlayersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Player player) {
                if (mPlayerListItemListener != null) {
                    mPlayerListItemListener.onPlayerClicked(player);
                }
            }
        });
    }

    @Override
    public void setListItemListener(PlayerListItemListener listener) {
        mPlayerListItemListener = listener;
    }

    @Override
    public void setPlayersControlListener(PlayersControlListener listener) {
        mPlayersControlListener = listener;
    }

    @Override
    public View getRootView() {
        return mRootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }


}
