package com.orensharon.playersranking.model;

/**
 * Created by orensharon on 7/5/17.
 */

public class Player {

    private String playerId;
    private String playerName;
    private String playerPictureUrl;
    private int playerRank;

    public Player(String name, String pictureUrl) {
        this(null, name, pictureUrl, 0);
    }

    public Player(String id, String name, String pictureUrl, int rank) {
        playerId = id;
        playerName = name;
        playerPictureUrl = pictureUrl;
        playerRank = rank;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerPictureUrl() {
        return playerPictureUrl;
    }

    public void setPlayerPictureUrl(String playerPictureUrl) {
        this.playerPictureUrl = playerPictureUrl;
    }

    public int getPlayerRank() {
        return playerRank;
    }

    public void setPlayerRank(int playerRank) {
        this.playerRank = playerRank;
    }
}
