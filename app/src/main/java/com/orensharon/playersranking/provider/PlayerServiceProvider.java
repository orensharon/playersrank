package com.orensharon.playersranking.provider;

import com.orensharon.playersranking.controller.AddNewPlayerActivity;
import com.orensharon.playersranking.model.Player;

import java.util.List;

/**
 * Created by orensharon on 7/6/17.
 */

public interface PlayerServiceProvider {


    interface PlayersProviderListener {
        void onPlayersResult(List<Player> players);
    }

    interface ShowProviderListener {
        void onPlayerResult(Player player);
        void onPlayerRemovedResult();
        void onPlayerRankResult(int newRank);
    }

    interface AddPlayerProviderListener {
        void onPlayerAddedResult(String id);
    }


    void getPlayersList();
    void rankPlayer(Player player, String email);
    void addPlayer(Player player);
    void removePlayer(String id);
    void getPlayer(String id);

    void setPlayersProviderListener(PlayersProviderListener listener);
    void setShowProviderListener(ShowProviderListener listener);
    void setAddPlayerProviderListener(AddPlayerProviderListener listener);
}
