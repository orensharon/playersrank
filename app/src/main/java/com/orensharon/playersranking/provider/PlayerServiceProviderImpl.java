package com.orensharon.playersranking.provider;

import android.util.Log;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.orensharon.playersranking.model.Player;

import java.lang.reflect.Type;
import java.util.List;

import static com.orensharon.playersranking.Constants.ADD_PLAYER_URL;
import static com.orensharon.playersranking.Constants.GET_PLAYERS_LIST_URL;
import static com.orensharon.playersranking.Constants.GET_PLAYER_URL;
import static com.orensharon.playersranking.Constants.RANK_PLAYER_URL;
import static com.orensharon.playersranking.Constants.REMOVE_PLAYER_URL;

/**
 * Created by orensharon on 7/6/17.
 */

public class PlayerServiceProviderImpl implements PlayerServiceProvider {

    private final String TAG = getClass().getSimpleName();

    private ShowProviderListener mShowPlayerProviderListener;
    private PlayersProviderListener mPlayersProviderListener;
    private AddPlayerProviderListener mAddPlayerProviderListener;

    private RestServiceProvider mRestServiceProvider;
    private Player mCachedPlayer;

    private static PlayerServiceProviderImpl mInstance;

    private PlayerServiceProviderImpl() {
        mCachedPlayer = null;
        mRestServiceProvider = new RestServiceProviderImpl();
    }

    public static PlayerServiceProviderImpl getInstance() {
        if (mInstance == null) {
            mInstance = new PlayerServiceProviderImpl();
        }
        return mInstance;
    }

    @Override
    public void getPlayersList() {
        mRestServiceProvider.get(GET_PLAYERS_LIST_URL, new RestServiceProvider.ResponseListener<JSONArray>() {
            @Override
            public void onSuccess(JSONArray response) {
                if (mPlayersProviderListener != null) {
                    Type listType = new TypeToken<List<Player>>() {}.getType();
                    List<Player> players = new Gson().fromJson(response.toString(), listType);
                    mPlayersProviderListener.onPlayersResult(players);
                }
            }
        });
    }

    @Override
    public void rankPlayer(Player player, String email) {

        try {
            JSONObject json = new JSONObject(new Gson().toJson(player));
            json.put("email", email);
            mRestServiceProvider.post(RANK_PLAYER_URL, json, new RestServiceProvider.ResponseListener() {
                @Override
                public void onSuccess(Object response) {
                    if (mShowPlayerProviderListener != null) {
                        mShowPlayerProviderListener.onPlayerRankResult((int)response);
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void addPlayer(Player player) {
        try {
            String body =  new Gson().toJson(player);
            JSONObject json = new JSONObject(body);
            mRestServiceProvider.post(ADD_PLAYER_URL, json, new RestServiceProvider.ResponseListener() {
                @Override
                public void onSuccess(Object response) {
                    if (mAddPlayerProviderListener != null) {
                        mAddPlayerProviderListener.onPlayerAddedResult((String) response);
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void removePlayer(String id) {
        String url = String.format("%s/%s",REMOVE_PLAYER_URL, id);
        mRestServiceProvider.delete(url, new RestServiceProvider.ResponseListener() {
            @Override
            public void onSuccess(Object response) {
                if (mShowPlayerProviderListener != null) {
                    mShowPlayerProviderListener.onPlayerRemovedResult();
                }
            }
        });
    }

    @Override
    public void getPlayer(String id) {
        String url = String.format("%s/%s",GET_PLAYER_URL, id);
        mRestServiceProvider.get(url, new RestServiceProvider.ResponseListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject response) {
                if (mShowPlayerProviderListener != null) {
                    Player player = new Gson().fromJson(response.toString(), Player.class);
                    mShowPlayerProviderListener.onPlayerResult(player);
                }
            }
        });
    }

    @Override
    public void setPlayersProviderListener(PlayersProviderListener listener) {
        mPlayersProviderListener = listener;
    }

    @Override
    public void setShowProviderListener(ShowProviderListener listener) {
        mShowPlayerProviderListener = listener;
    }

    @Override
    public void setAddPlayerProviderListener(AddPlayerProviderListener listener) {
        mAddPlayerProviderListener = listener;
    }


}
