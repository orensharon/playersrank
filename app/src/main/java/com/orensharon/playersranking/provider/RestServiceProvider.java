package com.orensharon.playersranking.provider;

import org.json.JSONObject;

/**
 * Created by orensharon on 7/6/17.
 */

public interface RestServiceProvider {

    interface ResponseListener<T> {
        void onSuccess(T object);
    }

    void get(String url, ResponseListener listener);
    void post(String url, JSONObject body, ResponseListener listener);
    void delete(String url, ResponseListener listener);
}
