package com.orensharon.playersranking.provider;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orensharon.playersranking.model.Player;
import com.orensharon.playersranking.provider.mock.PlayerServerMock;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by orensharon on 7/6/17.
 */

public class RestServiceProviderImpl implements RestServiceProvider {

    @Override
    public void get(String url, RestServiceProvider.ResponseListener listener) {
        if (listener != null) {
            Object response = PlayerServerMock.get(url);
            listener.onSuccess(response);
        }
    }

    @Override
    public void post(String url, JSONObject body, RestServiceProvider.ResponseListener listener) {
        if (listener != null) {
            Object response = PlayerServerMock.post(url, body);
            listener.onSuccess(response);
        }
    }

    @Override
    public void delete(String url, RestServiceProvider.ResponseListener listener) {
        if (listener != null) {
            PlayerServerMock.delete(url.split("/")[2]);
            listener.onSuccess(null);
        }
    }
}
