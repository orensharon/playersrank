package com.orensharon.playersranking.provider.mock;

import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.orensharon.playersranking.Constants.GET_PLAYERS_LIST_URL;
import static com.orensharon.playersranking.Constants.RANK_PLAYER_URL;

/**
 * Created by orensharon on 7/6/17.
 */

public class PlayerServerMock {


    private final static int MOCK_PLAYERS_COUNT = 10;

    public static HashMap<String, String> mPlayersRanking = new HashMap<>();
    public static HashMap<String, Integer> mRanksCount = new HashMap<>();
    public static HashMap<String, Integer> mRanksSum = new HashMap<>();
    public static JSONArray mPlayers = new JSONArray();
    static {
        for (int i = 0; i <= MOCK_PLAYERS_COUNT; i++) {
            JSONObject json = new JSONObject();
            try {
                json.put("playerId", String.valueOf(i) + "_" + System.currentTimeMillis());
                json.put("playerName", "player-" + i);
                json.put("playerPictureUrl", "url-" + i);
                json.put("playerRank", 0);
                mPlayers.put(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static Object get(String url) {
        if (url.equals(GET_PLAYERS_LIST_URL)) {
            return getPlayers();
        }

        // Get user info
        return getPlayer(url.split("/")[2]);
    }

    public static Object post(String url, JSONObject body) {

        if (url.equals(RANK_PLAYER_URL)) {
            return rankUser(body);
        }

        return addNewPlayer(body);
    }

    private static Object rankUser(JSONObject body) {

        try {
            String id = body.getString("playerId");
            String email = body.getString("email");
            Log.e("tag", mPlayersRanking.toString());
            if (mPlayersRanking.get(email) != null &&  mPlayersRanking.get(email).equals(id)) {
                // Player already ranked by this user
                return -1;
            }
            int rank = body.getInt("playerRank");
            for (int i = 0; i <= mPlayers.length(); i++) {
                JSONObject player = mPlayers.getJSONObject(i);
                if (player.getString("playerId").equals(id)) {

                    int totalRanks = mRanksCount.get(id) == null? 0: mRanksCount.get(id);
                    int totalRankSum = mRanksSum.get(id) == null? 0: mRanksSum.get(id);

                    mRanksCount.put(id, totalRanks +1);

                    int newRankSum = totalRankSum + rank;
                    mRanksSum.put(id, newRankSum);

                    int newRank = newRankSum / mRanksCount.get(id);
                    mPlayers.getJSONObject(i).put("playerRank", newRank);
                    mPlayersRanking.put(email, id);
                    return newRank;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    private static Object addNewPlayer(JSONObject body) {
        try {
            body.put("playerId", String.valueOf(mPlayers.length() + "_" + System.currentTimeMillis()));
            mPlayers.put(body);
            return body.get("playerId");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void delete(String id) {
        try {
            for (int i = 0; i <= mPlayers.length(); i++) {
                JSONObject player = mPlayers.getJSONObject(i);
                if (player.getString("playerId").equals(id)) {
                    mPlayers.remove(i);
                    return;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static JSONArray getPlayers() {
        return mPlayers;
    }

    private static JSONObject getPlayer(String id) {
        try {
            for (int i = 0; i <= mPlayers.length(); i++) {
                JSONObject player = mPlayers.getJSONObject(i);
                if (player.getString("playerId").equals(id)) {
                    return player;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
