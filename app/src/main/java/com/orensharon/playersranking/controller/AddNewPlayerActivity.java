package com.orensharon.playersranking.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.orensharon.playersranking.R;
import com.orensharon.playersranking.model.Player;
import com.orensharon.playersranking.provider.PlayerServiceProvider;
import com.orensharon.playersranking.provider.PlayerServiceProviderImpl;
import com.orensharon.playersranking.view.NewPlayerView;
import com.orensharon.playersranking.view.NewPlayerViewImpl;

public class AddNewPlayerActivity extends AppCompatActivity implements
        NewPlayerView.AddNewPlayerClickListener,
        PlayerServiceProvider.AddPlayerProviderListener {

    private NewPlayerView mNewPlayerView;
    private PlayerServiceProvider mPlayerServiceProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_player);

        mNewPlayerView = new NewPlayerViewImpl(
                LayoutInflater.from(this),
                (ViewGroup) findViewById(android.R.id.content)
        );

        mNewPlayerView.setListener(this);
        mPlayerServiceProvider = PlayerServiceProviderImpl.getInstance();
        mPlayerServiceProvider.setAddPlayerProviderListener(this);

    }

    @Override
    public void onAddNewPlayerClicked(String name, String picture) {
        Player player = new Player(name, picture);
        mPlayerServiceProvider.addPlayer(player);
    }

    @Override
    public void onPlayerAddedResult(String id) {
        Toast.makeText(this, "Player id: " + id + " added", Toast.LENGTH_LONG).show();
        finish();
    }

}
