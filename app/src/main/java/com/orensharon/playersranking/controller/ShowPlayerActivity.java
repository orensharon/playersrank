package com.orensharon.playersranking.controller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.orensharon.playersranking.R;
import com.orensharon.playersranking.model.Player;
import com.orensharon.playersranking.provider.PlayerServiceProvider;
import com.orensharon.playersranking.provider.PlayerServiceProviderImpl;
import com.orensharon.playersranking.view.ShowPlayerView;
import com.orensharon.playersranking.view.ShowPlayerViewImpl;

import static com.orensharon.playersranking.Constants.PLAYER_ID_EXTRA;

public class ShowPlayerActivity extends AppCompatActivity implements
        ShowPlayerView.ShowPlayerClickEventsListener,
        PlayerServiceProvider.ShowProviderListener {

    private ShowPlayerView mShowPlayerView;
    private String mPlayerId;
    private PlayerServiceProvider mPlayerServiceProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_player);

        mShowPlayerView = new ShowPlayerViewImpl(
                LayoutInflater.from(this),
                (ViewGroup) findViewById(android.R.id.content)
        );

        mShowPlayerView.setListener(this);
        mPlayerServiceProvider = PlayerServiceProviderImpl.getInstance();
        mPlayerServiceProvider.setShowProviderListener(this);
        readPlayerId();
    }

    @Override
    public void onRankPlayerClicked(Player player, String email) {
        if (player.getPlayerRank() < 0 || player.getPlayerRank() > 5 || email.isEmpty()) {
            // Input is not valid
            Toast.makeText(getApplicationContext(), "Input not valid", Toast.LENGTH_LONG).show();
            return;
        }
        mPlayerServiceProvider.rankPlayer(player, email);
    }

    @Override
    public void onPlayerRemovedClicked(String playerId) {
        if (playerId == null) {
            Toast.makeText(getApplicationContext(), "Error delete player", Toast.LENGTH_LONG).show();
            return;
        }
        mPlayerServiceProvider.removePlayer(playerId);
    }

    @Override
    public void onPlayerResult(Player player) {
        if (playerValidation(player)) {
            mShowPlayerView.bindPlayer(player);
        }
    }

    @Override
    public void onPlayerRemovedResult() {
        finish();
    }

    @Override
    public void onPlayerRankResult(int newRank) {
        if (newRank == -1) {
            Toast.makeText(this, "You have already ranked this player", Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(this, "Player ranked", Toast.LENGTH_LONG).show();
        finish();
    }

    private void readPlayerId() {
        mPlayerId = getIntent().getStringExtra(PLAYER_ID_EXTRA);
        if (!playerValidation(mPlayerId)) {
            finish();
        } else {
            mPlayerServiceProvider.getPlayer(mPlayerId);
        }
    }

    private boolean playerValidation(Object object) {
        if (object == null) {
            Toast.makeText(this, "Player loading error", Toast.LENGTH_LONG).show();
        }
        return object != null;
    }

}
