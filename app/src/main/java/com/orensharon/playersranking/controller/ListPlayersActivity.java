package com.orensharon.playersranking.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.orensharon.playersranking.R;
import com.orensharon.playersranking.model.Player;
import com.orensharon.playersranking.provider.PlayerServiceProvider;
import com.orensharon.playersranking.provider.PlayerServiceProviderImpl;
import com.orensharon.playersranking.view.ListPlayersView;
import com.orensharon.playersranking.view.ListPlayersViewImpl;

import java.util.List;

import static com.orensharon.playersranking.Constants.PLAYER_ID_EXTRA;

public class ListPlayersActivity extends AppCompatActivity implements
        ListPlayersView.PlayersControlListener, ListPlayersView.PlayerListItemListener,
        PlayerServiceProvider.PlayersProviderListener {

    private ListPlayersView mListPlayersView;
    private PlayerServiceProvider mPlayerServiceProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_player);

        mListPlayersView = new ListPlayersViewImpl(
                LayoutInflater.from(this),
                (ViewGroup) findViewById(android.R.id.content)
        );

        mListPlayersView.setListItemListener(this);
        mListPlayersView.setPlayersControlListener(this);


        mPlayerServiceProvider = PlayerServiceProviderImpl.getInstance();
        mPlayerServiceProvider.setPlayersProviderListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        // Get players from server
        mPlayerServiceProvider.getPlayersList();
    }

    @Override
    public void onPlayerClicked(Player player) {
        // Open player details
        Intent intent = new Intent(this, ShowPlayerActivity.class);
        intent.putExtra(PLAYER_ID_EXTRA, player.getPlayerId());
        startActivity(intent);
    }

    @Override
    public void addNewPlayerClickListener() {
        // Open new player activity
        Intent intent = new Intent(this, AddNewPlayerActivity.class);
        startActivity(intent);
    }


    @Override
    public void onPlayersResult(List<Player> players) {
        if (players == null) {
            Toast.makeText(this, "Players loading error", Toast.LENGTH_LONG).show();
            return;
        }
        mListPlayersView.bindPlayers(players);
    }
}
