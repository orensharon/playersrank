package com.orensharon.playersranking.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.orensharon.playersranking.R;
import com.orensharon.playersranking.model.Player;
import com.orensharon.playersranking.view.ListPlayersViewImpl;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.orensharon.playersranking.Constants.AVATAR_URL;

/**
 * Created by orensharon on 7/5/17.
 */

public class ListPlayersAdapter extends RecyclerView.Adapter<ListPlayersAdapter.CustomViewHolder> {


    private OnItemClickListener mOnItemClickListener;
    public interface OnItemClickListener {
        void onItemClick(Player player);
    }


    private List<Player> mPlayers;
    private Context mContext;

    public ListPlayersAdapter(Context context, List<Player> players) {
        mPlayers = players;
        mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_player_row, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder viewHolder, int position) {
        final Player player = mPlayers.get(position);
        viewHolder.playerName.setText(player.getPlayerName());

        Picasso.with(mContext)
                .load(AVATAR_URL)
                .into(viewHolder.avatar);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(player);
                }
            }
        });
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return (null != mPlayers ? mPlayers.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView avatar;
        protected TextView playerName;

        public CustomViewHolder(View view) {
            super(view);
            avatar = (ImageView) view.findViewById(R.id.avatar);
            playerName = (TextView) view.findViewById(R.id.player_name);
        }
    }
}
